<?php

class ServiceFactory {

    /**
     * Mapping classes
     * @var array
     */
    private static $map = array(
        "youtube" => "YoutubeService",
        "rutube" => "RutubeService",
    );

    /**
     * Build fabric classes
     * @param string $url
     * @return mixed
     * @throws Exception
     */
    public static function build($url) {
        $host =  parse_url($url, PHP_URL_HOST);

        foreach(self::$map as $key => $value) {
            if (preg_match("/" . $key . "/", $host)) {
                $className = $value;
                break;
            }
        }

        if (class_exists($className)) {
            return new $className($url);
        } else {
            throw new Exception("Данный видео хостинг не поддерживается");
        }
    }

}