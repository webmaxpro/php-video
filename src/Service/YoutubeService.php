<?php

class YoutubeService extends AbstractService {

    /**
     * YoutubeService constructor.
     * @param $url
     * @throws Exception
     */
    public function __construct($url)
    {
        $videoID = $this->getVideoIdFromURL($url);
        $urlParsed = "http://www.youtube.com/get_video_info?el=info&hl=en_US&eurl=&video_id=";

        $this->videoData = $this->dataParsedFromUrl(new CurlProvider(), $urlParsed, $videoID);

        if($this->videoData['status'] == 'fail') {
            throw new Exception("Такого видео не существует!");
        }
    }

    /**
     * @param object $provider
     * @param string $url
     * @param string $videoID
     * @return array
     */
    public function dataParsedFromUrl($provider, $url, $videoID) {
        $dataFromProvider = $provider->fetch($url . $videoID);
        $dataDecoded = $this->dataDecode($dataFromProvider);
        parse_str($dataDecoded, $data);

        return $data;
    }

    /**
     * @param string $data
     * @return string
     */
    public function dataDecode($data) {
        return urldecode($data);
    }

    /**
     * @param string $url
     * @return array
     */
    public function getVideoIdFromURL($url){
        $url_parts = parse_url($url);
        parse_str($url_parts['query'], $query_parts);

        return $query_parts['v'];
    }

    /**
     * Get title video
     * @return string
     */
    public function getTitle() {
        return $this->videoData['title'];
    }

    /**
     * Get video preview
     * @return string
     */
    public function getVideoPreview() {
        return $this->videoData['thumbnail_url'];
    }

    /**
     * Get video duration
     * @return string
     */
    public function getDuration() {
        return $this->videoData['length_seconds'];
    }

}