<?php

abstract class AbstractService {

    /* Video data */
    protected $videoData;

    /* Get video id from url */
    abstract public function getVideoIdFromURL($url);

    /* Get video title */
    abstract public function getTitle();

    /* Get video preview */
    abstract public function getVideoPreview();

    /* Get video duration */
    abstract public function getDuration();

}