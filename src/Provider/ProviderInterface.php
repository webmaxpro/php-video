<?php

interface ProviderInterface
{

    public function fetch($url);

}