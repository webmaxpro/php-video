<?php

require_once __DIR__.'/vendor/autoload.php';

$url = $_POST['url'];
if(isset($url)) {
    try {
        $object = ServiceFactory::build($url);
        $title = $object->getTitle();
        $videoPreview = $object->getVideoPreview();
        $duration = $object->getDuration();
    } catch(Exception $e) {
        $error = $e->getMessage();
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Maksim Melnichuk">
    <link rel="icon" href="favicon.ico">

    <title>Информация о видео</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>

<div class="container" >
    <div class="row">
        <div class="col-md-12">
            <h1>Информация о видео</h1>
            <form action="index.php" method="post">
                <div class="input-group">
                    <input type="text" name="url" value="<?=$url?>" class="form-control" placeholder="Введите URL видео" required>
                    <span class="input-group-btn">
                        <input class="btn btn-default" type="submit" value="Получить характеристики">
                    </span>
                </div>
            </form>
            <?php if(isset($error)): ?>
            <div class="alert alert-danger">
                <?=$error?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if(isset($title) || isset($videoPreview) || isset($duration)): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                Название: <?=$title?><br>
                Ссылка на превью: <?=$videoPreview?><br>
                Длительность: <?=$duration?> секунды или <?=date('i:s', $duration)?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

</body>
</html>
